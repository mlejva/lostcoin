let COINS = `XAI AICoin
ARCH ARCHcoin
AEON Aeon
ANC Anoncoin
ARDR Ardor
ADC AudioCoin
REP Augur
AUR Auroracoin
BANX Banx
BAT Basic Attention Token
BTA Bata
BAY BitBay
BSD BitSend
BTS BitShares
XBS BitStake
BTC Bitcoin
BCH Bitcoin Cash
BTCD BitcoinDark
SWIFT Bitswift
BC BlackCoin
BRK Breakout
BRX Breakout Stake
BCN Bytecoin
CLAM Clams
CLOAK CloakCoin
XCP Counterparty
DAO DAO
DCT DECENT
NOTE DNotes
DASH Dash
DCR Decred
DMD Diamond
DGB DigiByte
DGD DigixDAO
DGX DigixGOLD
DOGE Dogecoin
EOS EOS
EMC Emercoin
ETH Ethereum
ETC Ethereum Classic
ERC EuropeCoin
FCT Factorm
FTC Feathercoin
FLO FlorinCoin
FRK Franko
FC2 FuelCoin
GAME GameCredits
GEMZ GetGems
GNO Gnosis
GLD GoldCoin
GNT Golem
GOLOS Golos
GDC GrandCoin
GRC GridCoin
GRS Groestlcoin
NLG Gulden
HEAT HEAT
IOC I/O Coin
IOTA IOTA
ICN Iconomi
INCNT Incent
IFC Infinitecoin
JBS JumBucks
KOBO Kobocoin
KMD Komodo
KORE Kore
LBC LBRY Credits
LISK Lisk
LDOGE LiteDoge
LTC Litecoin
MAID MaidSafeCoin
MTR MasterTraderCoin
MSC Mastercoin
MINT Mintcoin
MONA MonaCoin
MCO Monaco
XMR Monero
MUE MonetaryUnit
XEM NEM
NEO NEO
NMC Namecoin
NEOS NeosCoin
NEU NeuCoin
NVC Novacoin
NBT NuBits
NXT Nxt
OK OKCoin
OMG OmiseGO
OMNI Omni
OPAL Opal
PIVX PIVX
PART Particl
PPC Peercoin
PIGGY Piggycoin
PINK PinkCoin
POT PotCoin
XPM Primecoin
QRK Quark
RADS Radium
RDD Reddcoin
RBT Rimbit
XRP Ripple
RISE Rise
RBIES Rubies
RBY Rubycoin
SAR SARCoin
SLS SaluS
SCOT Scotcoin
SDC ShadowCash
SIA Siacoin
STR StarCoin
START Startcoin
STEEM Steem
SLG Sterlingcoin
SJCX Storjcoin X
STRAT Stratis
UNITY SuperNET
SYNC Sync
AMP Synereo
SNRG Synergy
SYS Syscoin
USDT Tether
XTZ Tezos
TX TransferCoin
TRIG Triggers
UBQ Ubix
VPN VPNCoin
VNL VanillaCoin
XVG Verge
VRC VeriCoin
VTC Vertcoin
VIOR Viorcoin
WAVES Waves
YBC YBCoin
ZEC Zcash
ZEIT Zeitcoin
MRC microCoin`

const LOSS_CATEGORIES = {
    0: "Loss category",
    1: "💹 Lost on exchange",
    2: "🔫 Robbed",
    3: "🙅‍♀ Bad trade",
    4: "📞 Margin call",
    5: "💸 Wallet lost",
    6: "📈 Bought High, 📉 Sold Low",
    7: "👻 FOMO buy",
    8: "😱 FUD sell"
}

const DB_POSTS_COLLECTION_NAME = "posts";
const BATCH_SIZE = 16;

const DB_COIN_TAGS_COLLECTION_NAME = "coin-tags";
const DB_COINS_LOST_COLLECTION_NAME = "coins-lost";

const DB_USERS_COLLECTION_NAME = "users";

const signInButton          = document.getElementById("sign-in-button");
const signOutButton         = document.getElementById("sign-out-button");

const sortDropdown              = document.getElementById("sort-dropdown");
const noResultsMessage          = document.getElementById("no-results-message");
const websiteErrorMessage       = document.getElementById("website-error-message");
const submitModal               = document.getElementById("submit-modal");
const signInModal               = document.getElementById("sign-in-modal");
const modalClose                = document.getElementsByClassName("close-modal");
const lossCategoryTags          = document.getElementById("loss-category-tags");
const lossCategoryTagsSelected  = document.getElementById("loss-category-tags-selected");
const coinTagsSelected          = document.getElementById("coin-tags-selected");
const coinTags                  = document.getElementById("coin-tags");
const cardsLoading              = document.getElementById("cards-loading");
let coinCards                   = document.getElementById("coin-cards");

const form                  = document.getElementById("form-loss");
const coinSelect            = document.getElementById("coin-select");
const yearSelect            = document.getElementById("year-select");
const lossCategorySelect    = document.getElementById("loss-category-select");
const lostAmount            = document.getElementById("lost-amount");
const lostHow               = document.getElementById("lost-how");
const formErrorMessage      = document.getElementById("form-error-message");
const submitFormButton      = document.getElementById("submit-form-button");

let lastDatabaseObject = { };
// One entry for each new coin type
let tagElementsAddedToDOM = [ ];
let activeCoinTags = [ ];

let activeLossCategoryTagIDs = [ ];

let distanceFromBottom;
let isPollingForData = true;
let lastVisibleDocument;
let cardIndex = 0;

let lostCoinsObject = { };
/* ---------- */

/* Firebase Firestore initialization */
const config = {
    apiKey: "AIzaSyDT4nu-aEerzlzv6BgI63uWndaiq-Jd8zE ",
    projectId: "oh-my-coins",
    authDomain: "oh-my-coins.firebaseapp.com",
    databaseURL: "https://oh-my-coins.firebaseio.com"
};
firebase.initializeApp(config);
let db = firebase.firestore();
/* ---------- */

/* Firebase sign in init */
// FirebaseUI config.
/* const uiConfig = {
    signInSuccessUrl: '/',
    signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    ],
    // Terms of service url.
    tosUrl: ''
};
// Initialize the FirebaseUI Widget using Firebase.
const ui = new firebaseui.auth.AuthUI(firebase.auth());
// The start method will wait until the DOM is loaded.
ui.start('#firebaseui-auth-container', uiConfig); */

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        signOutButton.style.display = "block";
        signInButton.style.display = "none";
        // User is signed in.

        // TODO: Twitter email
        const displayName = user.displayName;
        const email = user.email;
        const uid = user.uid;

        // Log user to the database
        db.collection(DB_USERS_COLLECTION_NAME).doc(uid).get()
        .then(function(snapshot) {
            if (!snapshot.exists) {
                db.collection(DB_USERS_COLLECTION_NAME).doc(uid).set({
                    displayName: displayName,
                    email: email,
                    uid: uid
                });
            }
        });
    } else {
        signInButton.style.display = "block";
        signOutButton.style.display = "none";
    }
});
/* ---------- */

document.addEventListener("scroll", function() {
    // TODO: Infinite scroll works only when not filtering coins
    if (activeCoinTags.length == 0 && activeLossCategoryTagIDs.length == 0) {
        distanceFromBottom = getDistanceFromBottom();

        if (!isPollingForData && distanceFromBottom > 0 && distanceFromBottom <= 500) {
            isPollingForData = true;
            getNextBatch();
        }
    }
});

window.addEventListener("DOMContentLoaded", function(e) {
    signOutButton.style.display = "none";
    websiteErrorMessage.style.display = "none";

    addLossCategoryTags();

    // Get tags
    getCoinTags();

    // Get first batch
    getDatabaseData();

    // Clean form
    formErrorMessage.style.display = "none";
    lostAmount.value = "0";
    lostHow.value = "";
});

document.getElementById("burger").onclick = function(e) {
    e.currentTarget.classList.toggle("is-active");
    document.getElementById("navbar-buttons").classList.toggle("is-active");
}
document.getElementById("new-submit-button").onclick = function(e) {
    submitModal.classList.add("is-active");
};
signInButton.onclick = function(e) {
    signInModal.classList.add("is-active");
};
signOutButton.onclick = function(e) {
    firebase.auth().signOut().then(function() {
        console.log('Signed Out');
    }, function (error) {
        console.error('Sign Out Error', error);
    });
};

for (let i = 0; i < modalClose.length; i++) {
    modalClose[i].onclick = function(e) {
        submitModal.classList.remove("is-active");
        signInModal.classList.remove("is-active");
    };
}

/* Sort dropdown menu*/
document.onclick = function(e) {
    // Anything that gets to the document will hide the dropdown
    sortDropdown.classList.remove("is-active");
}
sortDropdown.onclick = function(e) {
    // Clicks within the dropdown won't make it past the dropdown itself
    e.stopPropagation();

    e.currentTarget.classList.toggle("is-active");
}
let sortDropdownItems = document.getElementsByClassName("sort-dropdown-item");
for (let i = 0; i < sortDropdownItems.length; i++) {
    const item = sortDropdownItems[i];
    item.onclick = function(e) {
        // Remove is-active from previously selected item
        for (let i = 0; i < sortDropdownItems.length; i++) {
            sortDropdownItems[i].classList.remove("is-active");
        }
        item.classList.add("is-active");
        document.getElementById("sort-dropdown-description").innerText = item.innerText;

        // Load data with different sort
        if (item.id === "sort-newest") {
            getDatabaseData();
        }
        else if (item.id === "sort-highest-loss") {
            // Convert coin cards to array (because sort())
            let cardsArray = Array.prototype.slice.call(document.getElementsByClassName("coin-card"));

            removeCoinCards();

            // Sort coin cards elements according to their USD loss
            cardsArray.sort(function (a, b) {
                return +b.dataset.loss - +a.dataset.loss;
            });

            // Put back in DOM
            for (let i = 0; i < cardsArray.length; i++) {
                coinCards.appendChild(cardsArray[i]);
            }

            // Hide loading indicator
            cardsLoading.style.display = "none";
        }
    };
}
/* ---------- */



// Add coin options to submit form
const lines = COINS.split("\n");
for (let i = 0; i < lines.length; i++) {
    let line = lines[i];

    let coinName = line.substr(line.indexOf(" ") + 1);
    let coinAbbreviation = line.substr(0, line.indexOf(" "));

    // Submit form
    let selectOption = document.createElement("option");
    selectOption.innerText = coinName;
    coinSelect.appendChild(selectOption);
}


// Generate years for submit form
/* let selectWhenYear = document.getElementById("year-select"); */
for (let year = 2009; year <= (new Date()).getFullYear(); year++) {
    let selectOption = document.createElement("option");
    selectOption.innerText = year;

    yearSelect.appendChild(selectOption);
}

// Add loss category options for submit form
for (const lossCategoryID in LOSS_CATEGORIES) {
    if (LOSS_CATEGORIES.hasOwnProperty(lossCategoryID)) {
        const lossCategoryText = LOSS_CATEGORIES[lossCategoryID];

        const selectOption = document.createElement("option");
        selectOption.innerText = lossCategoryText;
        selectOption.setAttribute("value", lossCategoryID);

        lossCategorySelect.appendChild(selectOption);
    }
}

// Check whether submit button is in range and how-textarea isn't empty
lostAmount.onkeyup = validateForm;
lostAmount.onchange = validateForm;
lostHow.onkeyup = validateForm;

// User wants to send form
submitFormButton.onclick = function(e) {
    e.preventDefault();
    formErrorMessage.style.display = "none";

    let url = "https://script.google.com/macros/s/AKfycbyjZVLONaDoqQghyWZe2GOFDu062xwKimBO7C9sfCEaenT7PQLR/exec";

    const selectedCoin = coinSelect.options[coinSelect.selectedIndex].value;

    let amount = lostAmount.value;
    amount = amount.replace(/</g, "&lt;").replace(/>/g, "&gt;");

    const year = yearSelect.options[yearSelect.selectedIndex].value;

    let how = lostHow.value;
    how = how.replace(/</g, "&lt;").replace(/>/g, "&gt;");

    const lossCategoryID = lossCategorySelect.options[lossCategorySelect.selectedIndex].getAttribute("value");

    // Prepare object for request
    let jsonObj = { };
    jsonObj.coin = selectedCoin;
    jsonObj.amount = amount;
    if (year !== "Year")
        jsonObj.year = year;
    else
        jsonObj.year = "";
    jsonObj.how = how;
    jsonObj.lossCategoryID = lossCategoryID;
    jsonObj.timestamp = firebase.firestore.FieldValue.serverTimestamp();

    submitFormButton.disabled = true;
    submitFormButton.classList.add("is-loading");

    submitFormData(jsonObj);
}

/* FUNCTIONS */
function switchFormToErrorState() {
    formErrorMessage.style.display = "block";
    submitFormButton.disabled = true;
    submitFormButton.classList.remove("is-loading");
}

function submitFormData(jsonDataObj) {
    db.collection("posts").add(jsonDataObj)
    .then(function (docRef) {
        // Success
        location.reload();
    })
    .catch(function (error) {
        switchFormToErrorState();
    });

    let coinAbbreviation = getCoinAbbreviation(jsonDataObj.coin);
    // Add coin tag to database
    addCoinToDatabase(jsonDataObj.coin);
    saveCoinLostAmount(coinAbbreviation, jsonDataObj.amount);
}

function addCoinToDatabase(coinName) {
    const coinAbbreviation = getCoinAbbreviation(coinName);
    db.collection(DB_COIN_TAGS_COLLECTION_NAME).doc(coinAbbreviation).set({ name: coinName, abbreviation: coinAbbreviation });
}

// Checks whether amount is in range and "lost-how" isn't empty
function validateForm(e) {
    const maxAmount = 100000;
    const submitFormButton = document.getElementById("submit-form-button");
    const lostAmountValue = Number(lostAmount.value);
    const lostHowValue = lostHow.value;

    if ((lostAmountValue > 0) && (lostAmountValue <= maxAmount) && (lostHowValue !== "")) {
        submitFormButton.disabled = false;
    }
    else {
        submitFormButton.disabled = true;
    }
}

function getCoinValueInUsdBtc(coin, cardIndex, callback) {
    let coinApiName = coin.replace(/\s+/g, '-').toLowerCase();
    let url = `https://api.coinmarketcap.com/v1/ticker/${coinApiName}/`

    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            let data = JSON.parse(request.responseText);
            if (data[0]["price_usd"]) {
                callback(data[0]["price_usd"], data[0]["price_btc"], cardIndex);
            }
        }
        else {
            // Coin is not on Coin Market Cap
        }
    };
    request.onerror = function () {
        // Request error
    };
    request.send();
}

function getCoinAbbreviation(coinName) {
    const lines = COINS.split('\n');

    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        if (line.indexOf(coinName) !== -1) {
            const coinAbbreviation = line.substr(0, line.indexOf(' '));
            return coinAbbreviation;
        }
    }
}

function getCoinName(coinAbbreviation) {
    let lines = COINS.split('\n');

    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        if (line.indexOf(coinAbbreviation) !== -1) {
            const coinName = line.substr(line.indexOf(' ') + 1);
            return coinName;
        }
    }
}

// Get initial batch
function getDatabaseData() {
    // Enable loading indicator
    cardsLoading.style.display = "block";

    noResultsMessage.style.display = "none";

    // Reset sums
    document.getElementById("total-loss-usd").innerText = "";
    document.getElementById("total-loss-btc").innerText = "";

    lossCategoryTagCoins = [];

    getTotalLoss();

    removeCoinCards();

    if (activeCoinTags.length > 0 || activeLossCategoryTagIDs.length > 0) {
        if (activeCoinTags.length > 0) {
            // Firestore doesn't support OR queries
            // -> for each coin filter create separate query
            let queryPromises = [];
            activeCoinTags.forEach(function(coinName) {
                queryPromises.push(
                    db.collection(DB_POSTS_COLLECTION_NAME).orderBy("timestamp", "desc").where("coin", "==", coinName).get()
                );
            });

            // Get it all at once
            Promise.all(queryPromises)
                .then(function (finishedQueries) {
                    // Go through each query and collect snapshots
                    // then parse it all together
                    let docs = [];
                    finishedQueries.forEach(function (snapshot) {
                        // Append all items from snapshot.docs onto docs
                        Array.prototype.push.apply(docs, snapshot.docs);
                    });
                    parseDocuments(docs);
                    cardsLoading.style.display = "none";
                })
                .catch(function (error) {
                    console.log(error);
                    websiteErrorMessage.style.display = "block";
                });
        }
        else if (activeLossCategoryTagIDs.length > 0) {
            let queryPromises = [];
            activeLossCategoryTagIDs.forEach(function(lossCategoryID) {
                queryPromises.push(
                    db.collection(DB_POSTS_COLLECTION_NAME).orderBy("timestamp", "desc").where("lossCategoryID", "==", lossCategoryID).get()
                );
            });

            Promise.all(queryPromises)
                .then(function(finishedQueries) {
                    // Go through each query and collect snapshots
                    // then parse it all together
                    let docs = [];
                    finishedQueries.forEach(function (snapshot) {
                        // Append all items from snapshot.docs onto docs
                        Array.prototype.push.apply(docs, snapshot.docs);
                    });

                    parseDocuments(docs);

                    if (docs.length === 0) {
                        noResultsMessage.style.display = "block";
                    }

                    cardsLoading.style.display = "none";
                })
                .catch(function(error) {
                    console.log(error);
                    websiteErrorMessage.style.display = "block";
                });
        }
        else if (activeCoinTags.length > 0 && activeLossCategoryTagIDs.length > 0) {

        }
    }
    else {
        // Don't filter any cards
        const query = db.collection(DB_POSTS_COLLECTION_NAME).orderBy("timestamp", "desc").limit(BATCH_SIZE);
        query.get()
            .then(function (documentSnapshots) {
                lastVisibleDocument = documentSnapshots.docs[documentSnapshots.docs.length - 1];
                parseDocuments(documentSnapshots.docs);
                cardsLoading.style.display = "none";
                isPollingForData = false;
            })
            .catch(function (error) {
                console.log(error);
                websiteErrorMessage.style.display = "block";
            });
    }
}

function getNextBatch() {
    // Enable loading indicator
    cardsLoading.style.display = "block";

    const query = db.collection(DB_POSTS_COLLECTION_NAME).orderBy("timestamp", "desc").startAfter(lastVisibleDocument).limit(BATCH_SIZE);
    query.get()
    .then(function(documentSnapshots) {
        lastVisibleDocument = documentSnapshots.docs[documentSnapshots.docs.length - 1];
        parseDocuments(documentSnapshots.docs);
        cardsLoading.style.display = "none";
        isPollingForData = false;
    })
    .catch(function(error) {
        console.log(error);
        websiteErrorMessage.style.display = "block";
    });
}

function parseDocuments(snapshots) {
    for (let i = 0; i < snapshots.length; i++) {
        const data = snapshots[i].data();
        /* console.log(`${snapshots[i].id} - ${data["how"]}`); */
        const coin = data["coin"];
        const coinAbbreviation = getCoinAbbreviation(coin);

        const amount = data["amount"];
        let year = data["year"];
        let how = data["how"];

        if ((year === "undefined") || (year === "")) {
            year = "N/A";
        }
        if ((how === "undefined") || (how === "")) {
            how = "N/A";
        }


        // Create post card
        cardIndex += 1;

        const postCardColumn = document.createElement("div");
        postCardColumn.id = `card-${cardIndex}`;
        postCardColumn.className = "column is-one-third-desktop is-one-third-widescreen is-one-quarter-fullhd coin-card";

        const cardBulma = document.createElement("div");
        cardBulma.className = "card";

        const cardHeader = document.createElement("div");
        cardHeader.className = "card-header";
        cardHeader.innerHTML = `<div class="card-header-title">
                                    <div class="columns">
                                        <div class="column is-12">
                                            <i class="cc ${coinAbbreviation}"></i> ${coin}
                                        </div>
                                    </div>
                                </div>`;

        const cardContent = document.createElement("div");
        cardContent.className = "card-content";

        const columns = document.createElement("div");
        columns.className = "columns is-multiline";

        const columnYear = document.createElement("div");
        columnYear.className = "column is-12";
        columnYear.innerHTML = `<span class="description">🗓</span> ${year}`;
        columns.appendChild(columnYear);

        const columnAmount = document.createElement("div");
        columnAmount.className = "column is-12";
        columnAmount.innerHTML = `<span class="description">🔒</span> ${Number(amount).toLocaleString("en")} ${coinAbbreviation}`;
        columns.appendChild(columnAmount);

        const columnValue = document.createElement("div");
        columnValue.id = `loss-${cardIndex}-total-value`;
        columnValue.className = "column is-12";
        columnValue.innerHTML = `<span class="description">💰</span>`;
        columns.appendChild(columnValue);

        const lossCategoryID = data["lossCategoryID"];
        let lossCategoryText = "N/A";
        if (lossCategoryID > 0) {
            lossCategoryText = LOSS_CATEGORIES[lossCategoryID];

            const columnLossCategory = document.createElement("div");
            columnLossCategory.className ="column is-12";
            columnLossCategory.innerHTML = `${lossCategoryText}`
            columns.appendChild(columnLossCategory);
        }

        const columnHow = document.createElement("div");
        columnHow.className = "wasted-how column";
        columnHow.innerHTML = `${how}`;
        columns.appendChild(columnHow);


        cardContent.appendChild(columns);

        cardBulma.appendChild(cardHeader);
        cardBulma.appendChild(cardContent);
        postCardColumn.appendChild(cardBulma);

        coinCards.appendChild(postCardColumn);

        getCoinValueInUsdBtc(coin, cardIndex, function(valueUsd, valueBtc, cardIndex) {
            // Total loss for the given card
            let cardUsdLossValue = (amount * valueUsd).toFixed(0);
            const cardBtcLossValue = (amount * valueBtc).toFixed(3);


            // Find the card children and insert the usd card loss
            const coinCard = document.getElementById(`card-${cardIndex}`);
            coinCard.setAttribute("data-loss", cardUsdLossValue);
            cardUsdLossValue = Number(cardUsdLossValue).toLocaleString("en");
            document.getElementById(`loss-${cardIndex}-total-value`).innerText += ` ${cardUsdLossValue}`;
        });
    }
}

function removeCoinCards() {
    while (coinCards.firstChild) {
        coinCards.removeChild(coinCards.firstChild);
    }
}

function getCoinTags() {
    let query = db.collection(DB_COIN_TAGS_COLLECTION_NAME);
    query.get()
    .then(function(documentSnapshots) {
        for (let i = 0; i < documentSnapshots.docs.length; i++) {
            const data = documentSnapshots.docs[i].data();
            const coinName = data['name'];
            const coinAbbreviation = data['abbreviation'];

            // Add coin to addedCoinTags if that type isn't already there
            if (tagElementsAddedToDOM.indexOf(coinAbbreviation) === -1) {
                tagElementsAddedToDOM.push(coinAbbreviation);
                addCoinTag(coinAbbreviation, coinName);
            }
        }
    })
    .catch(function(error) {
        console.log(error);
    })
}

function addLossCategoryTags() {
    for (const lossCategoryID in LOSS_CATEGORIES) {
        if (LOSS_CATEGORIES.hasOwnProperty(lossCategoryID) && lossCategoryID > 0) {
            const lossCategoryText = LOSS_CATEGORIES[lossCategoryID];

            const newLossCategoryTag = document.createElement("span");
            newLossCategoryTag.id = `category-loss-tag-${lossCategoryID}`;
            newLossCategoryTag.className = "tag tag-choose is-rounded is-medium";
            newLossCategoryTag.setAttribute("category-id", lossCategoryID);

            newLossCategoryTag.innerText = lossCategoryText;
            newLossCategoryTag.onclick = function(e) {

                if (!e.currentTarget.classList.contains("tag-disabled")) {
                    // 1. Make this loss category tag disabled
                    e.currentTarget.classList.add("tag-disabled");

                    // 2. Remove current active loss category tag
                    activeLossCategoryTagIDs.forEach(lossCategoryID => {
                        const activeTag = document.getElementById(`selected-category-loss-tag-${lossCategoryID}`);
                        removeActiveLossCategoryTag(activeTag);
                    });

                    // 3. Remove all active coin tag because of uneffecient database querying
                    activeCoinTags.forEach(function(coinName) {
                        const coinAbbreviation = getCoinAbbreviation(coinName);
                        const activeTag = document.getElementById(`selected-tag-${coinAbbreviation}`);
                        removeActiveCoinTag(activeTag);
                    });

                    // 4. Make this loss category tag active
                    const thisTagCategoryID = e.currentTarget.getAttribute("category-id");
                    const thisTagCategoryText = e.currentTarget.innerText;
                    addActiveLossCategoryTag(thisTagCategoryID, thisTagCategoryText);

                    // 5. Refresh cards
                    getDatabaseData();
                }
                else {
                    const activeTag = document.getElementById(`selected-category-loss-tag-${lossCategoryID}`);
                    removeActiveLossCategoryTag(activeTag);

                    // Refresh cards
                    getDatabaseData();
                }
            }
            lossCategoryTags.appendChild(newLossCategoryTag);
        }
    }
}

function addActiveLossCategoryTag(lossCategoryID, lossCategoryText) {
    const selectedTag = document.createElement("span");
    selectedTag.className += "tag is-rounded is-medium";
    selectedTag.id = `selected-category-loss-tag-${lossCategoryID}`;
    selectedTag.setAttribute("category-id", lossCategoryID);

    const tagInner = document.createElement("span");
    tagInner.className = "selected-tag-inner";
    tagInner.innerText = lossCategoryText;

    const deleteButton = document.createElement("button");
    deleteButton.className += "delete is-small";
    deleteButton.onclick = function (e) {
        removeActiveLossCategoryTag(e.currentTarget.parentElement);

        // Refresh cards
        getDatabaseData();
    };

    selectedTag.appendChild(tagInner);
    selectedTag.appendChild(deleteButton);
    lossCategoryTagsSelected.appendChild(selectedTag);

    activeLossCategoryTagIDs.push(lossCategoryID);
}

function removeActiveLossCategoryTag(activeTagElement) {
    const lossCategoryID = activeTagElement.getAttribute("category-id");

    activeLossCategoryTagIDs = activeLossCategoryTagIDs.filter(function(activeLossCategoryID) {
        return lossCategoryID !== lossCategoryID;
    });
    activeTagElement.parentNode.removeChild(activeTagElement);

    // Now selected category loss tag can be active again
    // -> remove tag-disabled class
    document.getElementById(`category-loss-tag-${lossCategoryID}`).classList.remove("tag-disabled");
}

function addCoinTag(coinAbbreviation, coinName) {
    const newCoinTag = document.createElement("span");
    newCoinTag.className += "tag tag-choose is-rounded is-medium";
    newCoinTag.setAttribute("coin-short", coinAbbreviation);
    newCoinTag.setAttribute("coin-name", coinName);
    newCoinTag.id = `tag-${coinAbbreviation}`;
    const tagInner = document.createElement("span");
    tagInner.className += `cc ${coinAbbreviation}`;
    tagInner.innerText = ` ${coinName}`;

    newCoinTag.onclick = function(e) {
        const coinAbbreviation = e.currentTarget.getAttribute("coin-short");
        if (!e.currentTarget.classList.contains("tag-disabled")) {
            const coinName = e.currentTarget.getAttribute("coin-name");

            // Disable this tag so it can't be added again
            e.currentTarget.classList.add("tag-disabled");

            // Remove all active loss category tag because of uneffecient database querying
            activeLossCategoryTagIDs.forEach(lossCategoryID => {
                const activeTag = document.getElementById(`selected-category-loss-tag-${lossCategoryID}`);
                removeActiveLossCategoryTag(activeTag);
            });

            addActiveCoinTag(coinAbbreviation, coinName);

            // Refresh cards
            getDatabaseData();
        }
        else {
            const activeTag = document.getElementById(`selected-tag-${coinAbbreviation}`);
            removeActiveCoinTag(activeTag);

            // Refresh cards
            getDatabaseData();
        }
    }

    newCoinTag.appendChild(tagInner);
    coinTags.appendChild(newCoinTag);
}

function addActiveCoinTag(coinAbbreviation, coinName) {
    // Add coin tag to coin-tags-selected
    // to show user she's filtering by coin
    const selectedTag = document.createElement("span");
    selectedTag.className += "tag is-rounded is-medium";
    selectedTag.id = `selected-tag-${coinAbbreviation}`
    selectedTag.setAttribute("coin-name", coinName);
    const tagInner = document.createElement("span");
    tagInner.className = `cc ${coinAbbreviation} selected-tag-inner`;
    tagInner.innerText = ` ${coinName}`;
    const deleteButton = document.createElement("button");
    deleteButton.className += "delete is-small";
    deleteButton.onclick = function(e) {
        removeActiveCoinTag(e.currentTarget.parentElement);

        // Refresh cards
        getDatabaseData();
    };

    selectedTag.appendChild(tagInner);
    selectedTag.appendChild(deleteButton);
    coinTagsSelected.appendChild(selectedTag);

    activeCoinTags.push(coinName);
}

function removeActiveCoinTag(activeTagElement) {
    // Refresh array for coin filter
    const coinName = activeTagElement.getAttribute("coin-name");
    const coinAbbreviation = getCoinAbbreviation(coinName);
    activeCoinTags = activeCoinTags.filter(function(activeCoinName) {
        return activeCoinName !== coinName;
    });
    activeTagElement.parentNode.removeChild(activeTagElement);

    // Now select coin tag can be active again
    // -> remove tag-disabled class
    document.getElementById(`tag-${coinAbbreviation}`).classList.remove("tag-disabled");
}

function getDistanceFromBottom() {
    let scrollPosition = window.pageYOffset;
    let contentSize = document.querySelector('.content').offsetHeight;

    return Math.max(contentSize - scrollPosition, 0);
}

function saveCoinLostAmount(coinAbbreviation, amountLost) {
    // Read latest lost amount for the given coin
    let lostTotal = parseFloat(amountLost);
    const query = db.collection(DB_COINS_LOST_COLLECTION_NAME).doc(coinAbbreviation);
    query.get()
    .then(function(doc) {
        if (doc.exists) {
            const data = doc.data();
            const lostSoFar = parseFloat(data['lost']);
            console.log(`Coin document exists with ${lostSoFar} lost coin`);

            console.log(`\tAdding ${lostTotal}`);
            lostTotal += parseFloat(lostSoFar);
            console.log(`\tNow ${lostTotal}`);
            db.collection(DB_COINS_LOST_COLLECTION_NAME).doc(coinAbbreviation).set({ coin: coinAbbreviation, lost: lostTotal });
        }
        else {
            console.log(`Coin document for '${coinAbbreviation}' doesn't exist, creating new with amount ${lostTotal}`);

            // Write new
            db.collection(DB_COINS_LOST_COLLECTION_NAME).doc(coinAbbreviation).set({ coin: coinAbbreviation, lost: lostTotal });
        }
    })
    .catch(function(error) {
        console.log(error);
    });
}

function getTotalLoss() {
    const query = db.collection(DB_COINS_LOST_COLLECTION_NAME);
    query.get()
    .then(function(documentSnapshots) {
        const documents = documentSnapshots.docs;

        for (let i = 0; i < documents.length; i++) {
            const data = documents[i].data();

            const coinAbbreviation = data["coin"];
            const lostAmount = parseFloat(data['lost']);

            const coinName = getCoinName(coinAbbreviation);

            if ((activeCoinTags.length === 0) || (activeCoinTags.indexOf(coinName) !== -1)) {
                getCoinValueInUsdBtc(coinName, 0, function(valueUsd, valueBtc, cardIndex) {
                    const lostTotalInUsdForCoin = parseInt(valueUsd * lostAmount);
                    const lostTotalInBtcForCoin = parseFloat(valueBtc * lostAmount);

                    // Update a global counter - sum of losses of all cards in usd
                    const sumLossUsdElement = document.getElementById("total-loss-usd");
                    const sumLossUsdText = sumLossUsdElement.innerText.replace(/,/g, '');
                    let sumLossUsd = Number(sumLossUsdText);
                    sumLossUsd += Number(lostTotalInUsdForCoin);
                    sumLossUsdElement.innerHTML = sumLossUsd.toLocaleString("en");


                    // Update a global counter - sum of losses of all cards in btc
                    const sumLossBtcElement = document.getElementById("total-loss-btc");
                    const sumLossBtcText = sumLossBtcElement.innerText.replace(/,/g, '');
                    let sumLossBtc = Number(sumLossBtcText);
                    sumLossBtc += Number(lostTotalInBtcForCoin);
                    sumLossBtcElement.innerHTML = Number(sumLossBtc.toFixed(3)).toLocaleString("en");
                });
            }
        }
    })
    .catch(function(error) {
        console.log('Could not get total loss');
        console.log(error);
    })
}

