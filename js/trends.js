let COINS = `XAI AICoin
ARCH ARCHcoin
AEON Aeon
ANC Anoncoin
ARDR Ardor
ADC AudioCoin
REP Augur
AUR Auroracoin
BANX Banx
BAT Basic Attention Token
BTA Bata
BAY BitBay
BSD BitSend
BTS BitShares
XBS BitStake
BTC Bitcoin
BCH Bitcoin Cash
BTCD BitcoinDark
SWIFT Bitswift
BC BlackCoin
BRK Breakout
BRX Breakout Stake
BCN Bytecoin
CLAM Clams
CLOAK CloakCoin
XCP Counterparty
DAO DAO
DCT DECENT
NOTE DNotes
DASH Dash
DCR Decred
DMD Diamond
DGB DigiByte
DGD DigixDAO
DGX DigixGOLD
DOGE Dogecoin
EOS EOS
EMC Emercoin
ETH Ethereum
ETC Ethereum Classic
ERC EuropeCoin
FCT Factorm
FTC Feathercoin
FLO FlorinCoin
FRK Franko
FC2 FuelCoin
GAME GameCredits
GEMZ GetGems
GNO Gnosis
GLD GoldCoin
GNT Golem
GOLOS Golos
GDC GrandCoin
GRC GridCoin
GRS Groestlcoin
NLG Gulden
HEAT HEAT
IOC I/O Coin
IOTA IOTA
ICN Iconomi
INCNT Incent
IFC Infinitecoin
JBS JumBucks
KOBO Kobocoin
KMD Komodo
KORE Kore
LBC LBRY Credits
LISK Lisk
LDOGE LiteDoge
LTC Litecoin
MAID MaidSafeCoin
MTR MasterTraderCoin
MSC Mastercoin
MINT Mintcoin
MONA MonaCoin
MCO Monaco
XMR Monero
MUE MonetaryUnit
XEM NEM
NEO NEO
NMC Namecoin
NEOS NeosCoin
NEU NeuCoin
NVC Novacoin
NBT NuBits
NXT Nxt
OK OKCoin
OMG OmiseGO
OMNI Omni
OPAL Opal
PIVX PIVX
PART Particl
PPC Peercoin
PIGGY Piggycoin
PINK PinkCoin
POT PotCoin
XPM Primecoin
QRK Quark
RADS Radium
RDD Reddcoin
RBT Rimbit
XRP Ripple
RISE Rise
RBIES Rubies
RBY Rubycoin
SAR SARCoin
SLS SaluS
SCOT Scotcoin
SDC ShadowCash
SIA Siacoin
STR StarCoin
START Startcoin
STEEM Steem
SLG Sterlingcoin
SJCX Storjcoin X
STRAT Stratis
UNITY SuperNET
SYNC Sync
AMP Synereo
SNRG Synergy
SYS Syscoin
USDT Tether
XTZ Tezos
TX TransferCoin
TRIG Triggers
UBQ Ubix
VPN VPNCoin
VNL VanillaCoin
XVG Verge
VRC VeriCoin
VTC Vertcoin
VIOR Viorcoin
WAVES Waves
YBC YBCoin
ZEC Zcash
ZEIT Zeitcoin
MRC microCoin`


const navbarButtons         = document.getElementById("navbar-buttons");
const burgerNavbar          = document.getElementById("burger");
const tableLoading          = document.getElementById("table-loading");
const websiteErrorMessage   = document.getElementById("website-error-message");


const tableCoin             = document.getElementById("table-coin");
const tableCoinBody         = document.getElementById("table-coin-body");

const reloadTableButton     = document.getElementById("reload-table-button");

const tablePosition     = document.getElementById("table-position");
let positionSort = 1; // 0 - sorted from lower to higher

const tableName         = document.getElementById("table-name");
let nameSort = 0;   // 0 - Sort from 0-9-A-Z

const tableMarketCap    = document.getElementById("table-market-cap");
let marketCapSort = 0;  // 0 - sorted from higher to lower

const tablePrice        = document.getElementById("table-price");
let priceSort = 1; // 1 - sorted from higher to lower

const tableVolume       = document.getElementById("table-volume");
let volumeSort = 1;

const tableSupply       = document.getElementById("table-supply");
let supplySort = 1;

const tableChange       = document.getElementById("table-change");
let changeSort = 1;

let topCoins = [];
/* ---------- */

reloadTableButton.onclick = function(e) {
    /* clearTable();
    tableLoading.style.display = "block";

    populateTable(); */

    location.reload();
}

/* Sorting buttons */
tablePosition.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (positionSort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["rank"] > +a["rank"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        positionSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["rank"] > +a["rank"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        positionSort = 0;
    }

    // Inser each coin
    insertTopCoins();
}

tableName.onclick = function (e) {
    clearTable();

    if (nameSort === 0) {
        topCoins.sort(function (a, b) {
            if (b["name"].toLowerCase() > a["name"].toLowerCase()) {
                return -1
            }
            else {
                return 1;
            }
        });
        nameSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (b["name"].toLowerCase() > a["name"].toLowerCase()) {
                return 1
            }
            else {
                return -1;
            }
        });
        nameSort = 0;
    }

    insertTopCoins();
}

tableMarketCap.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (marketCapSort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["market_cap_usd"] > +a["market_cap_usd"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        marketCapSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["market_cap_usd"] > +a["market_cap_usd"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        marketCapSort = 0;
    }

    // Inser each coin
    insertTopCoins();
}

tablePrice.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (priceSort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["price_usd"] > +a["price_usd"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        priceSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["price_usd"] > +a["price_usd"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        priceSort = 0;
    }

    // Inser each coin
    insertTopCoins();
}

tableVolume.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (volumeSort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["24h_volume_usd"] > +a["24h_volume_usd"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        volumeSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["24h_volume_usd"] > +a["24h_volume_usd"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        volumeSort = 0;
    }

    // Inser each coin
    insertTopCoins();
}

tableSupply.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (supplySort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["available_supply"] > +a["available_supply"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        supplySort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["available_supply"] > +a["available_supply"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        supplySort = 0;
    }

    // Inser each coin
    insertTopCoins();
}

tableChange.onclick = function (e) {
    // Clean table
    clearTable();

    // Sort topCoins
    if (changeSort === 0) {
        topCoins.sort(function (a, b) {
            if (+b["percent_change_24h"] > +a["percent_change_24h"]) {
                return -1
            }
            else {
                return 1;
            }
        });
        changeSort = 1;
    }
    else {
        topCoins.sort(function (a, b) {
            if (+b["percent_change_24h"] > +a["percent_change_24h"]) {
                return 1
            }
            else {
                return -1;
            }
        });
        changeSort = 0;
    }

    // Inser each coin
    insertTopCoins();
}
/* ---------- */


burgerNavbar.onclick = function (e) {
    e.currentTarget.classList.toggle("is-active");
    navbarButtons.classList.toggle("is-active");
}

populateTable();

/* FUNCTIONS */
function populateTable() {
    getFirstHundredCoinsData(function(coinsArray) {
        topCoins = [];

        for (let i = 0; i < coinsArray.length; i++) {
            const coinData = coinsArray[i];
            topCoins.push(coinData);
            insertCoinToTable(coinData);
        }
    });
}

function insertTopCoins() {
    for (let i = 0; i < topCoins.length; i++) {
        const coinObject = topCoins[i];

        insertCoinToTable(coinObject);
    }
}

function clearTable() {
    tableCoinBody.innerHTML = '';
}

function insertCoinToTable(coinObject) {
    const rank = coinObject["rank"];
    const symbol = coinObject["symbol"];
    const name = coinObject["name"];
    const marketCap = coinObject["market_cap_usd"];
    const price = coinObject["price_usd"];
    const volume = coinObject["24h_volume_usd"];
    const supply = coinObject["available_supply"];
    const change = coinObject["percent_change_24h"];

    const tableDataRank = document.createElement("td");
    tableDataRank.innerText = rank;

    const tableDataName = document.createElement("td");
    tableDataName.innerHTML = `<span class="cc ${getCoinAbbreviation(name)}"> ${name}</span>`;
    if (name === "IOTA") {
        tableDataName.classList.add("IOTA");
    }

    const tableDataMarketCap = document.createElement("td");
    tableDataMarketCap.innerText = `$${Number(marketCap).toLocaleString("en")}`;

    const tableDataPrice = document.createElement("td");
    tableDataPrice.innerText = `$${Number(price).toLocaleString("en")}`;

    const tableDataVolume = document.createElement("td");
    tableDataVolume.innerText = `$${Number(volume).toLocaleString("en")}`;

    const tableDataSupply = document.createElement("td");
    tableDataSupply.innerText = `${Number(supply).toLocaleString("en")} ${symbol}`;

    const tableDataChange = document.createElement("td");
    if (Number(change) > 0)  {
        tableDataChange.classList.add("change-up");
    }
    else {
        tableDataChange.classList.add("change-down");
    }
    tableDataChange.innerText = `${Number(change)}%`;




    const tableRow = document.createElement("tr");
    tableRow.appendChild(tableDataRank);
    tableRow.appendChild(tableDataName);
    tableRow.appendChild(tableDataMarketCap);
    tableRow.appendChild(tableDataPrice);
    tableRow.appendChild(tableDataVolume);
    tableRow.appendChild(tableDataSupply);
    tableRow.appendChild(tableDataChange);


    tableCoinBody.appendChild(tableRow);
}

function getFirstHundredCoinsData(callback) {
    const url = 'https://api.coinmarketcap.com/v1/ticker/?limit=100';

    let request= new XMLHttpRequest();
    request.open("GET", url, true);
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            tableLoading.style.display = "none";
            let data = JSON.parse(request.responseText);
            callback(data);
        }
        else {
            // Server error
            tableLoading.style.display = "none";
            websiteErrorMessage.style.display = "block";
        }

    };
    request.onerror = function () {
        // Request error
        tableLoading.style.display = "none";
        websiteErrorMessage.style.display = "block";
    };
    request.send();
}

function getCoinAbbreviation(coinName) {
    const lines = COINS.split('\n');

    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        if (line.indexOf(coinName) !== -1) {
            const coinAbbreviation = line.substr(0, line.indexOf(' '));
            return coinAbbreviation;
        }
    }
}