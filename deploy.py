import boto3
import os.path
import sys

def upload_directory(src_dir, bucket_name, dst_dir):
    if not os.path.isdir(src_dir):
        raise ValueError('src_dir %r not found.' % src_dir)
    all_files = []

    for root, dirs, files in os.walk(src_dir):
        files = [f for f in files if (not f[0] == '.' and not f == 'deploy.py' and not f == 'rootkey.csv' and not f == 'coins.txt')]
        dirs[:] = [d for d in dirs if not d[0] == '.']
        all_files += [os.path.join(root, f) for f in files]
    s3_resource = boto3.resource('s3')
    for filename in all_files:
        print('Uploading {}'.format(filename))
        s3_resource.Object(bucket_name, os.path.join(dst_dir, os.path.relpath(filename, src_dir))).put(Body=open(filename, 'rb'))
    print("Done")

bucket_name = 'ohmycoins'
sourceDir = '.'
destDir = ''
upload_directory('.', bucket_name, '')
